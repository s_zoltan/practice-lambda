package main.java.runnable;
import main.java.services.*;
import main.java.examples.*;

public class LambdaPractice {
    public static int x = 5;
    public static void main(String[] args) {

        //First we start with a basic interface. One int parameter, and the return type is also int
        BasicLambdaInterface multiply = (n) -> n*10;
        System.out.println("Multiply "+x+" by 10: "+multiply.func(x));

        //Second interface is a Generic one. <T> specified in the interface, return type and parameter are also T.
        //Only reference types can be used, for primitive type we can use wrapper, like Float, Integer, etc.
        GenericLambdaInterface<Integer> factor = (n) -> {
            int result=0;
            for (int i=0; i<=n; i++) {
                result *= i;
            }
            return result;
        };
        System.out.println("\nFactorial of "+x+" is: "+factor.func(x));

        //Next we try a static method intOP where the parameter is the basic lamdba interface.
        //The operation can be declared directly
        System.out.println("\nMultiply using static method: "+ intOperation((n)->n*10,x));

        //or we can pass the previously declared multiply operation as an argument
        System.out.println("\nMultiply using argument pass: "+ intOperation(multiply,x));

        //Class methods can be used also, if the parameter and return type matches with the interface type.
        //multiplybyten method matches with BasicLambdaInterface and it works in intOP method.
        System.out.println("\nMethod reference using static method: "+ intOperation(MethodRefExampleStatic::multiplyByTen,x));


        //or non-static classes can be used, after instantiation
        MethodRefExampleNonStatic refExample = new MethodRefExampleNonStatic();
        System.out.println("\nMethod reference using instance: "+ intOperation(refExample::methodRef,x));

        //We can also use generic methods
        System.out.println("Method reference using generic static method: "+myOP(MethodRefGenericExample::multiplyByTen,x));
    }

    static int intOperation(BasicLambdaInterface a, int b) {
        return a.func(b);
    }

    static <T extends Number> int myOP(MethodRefGeneric<T> a, T b) {
        return a.func(b);
    }

}
