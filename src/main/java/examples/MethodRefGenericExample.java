package main.java.examples;

public class MethodRefGenericExample {
    public static <T extends Number> int multiplyByTen(T a) {
        return (int)a*10;
    }
}

