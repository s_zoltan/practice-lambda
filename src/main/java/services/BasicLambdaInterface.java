package main.java.services;

@FunctionalInterface
public interface BasicLambdaInterface {
    int func(int a);
}

