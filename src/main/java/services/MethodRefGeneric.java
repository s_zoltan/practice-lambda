package main.java.services;

public interface MethodRefGeneric<T extends Number> {
    int func(T a);
}
