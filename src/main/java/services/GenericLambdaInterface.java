package main.java.services;

public interface GenericLambdaInterface<T> {
    T func(T a);
}
